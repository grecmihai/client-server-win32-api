/*
	Comenzi acceptate:
	echo [string]
	register [username] [password]
	login [username] [password]
	logout
	msg [user] [message content]
	broadcast [message content]
	sendfile [user] [file path]
	list
	exit
	history [username] [count]
*/

#include "stdafx.h"
#include "constants.h"
#include <Windows.h>
#include <stdio.h>
#include <strsafe.h>

#define COMMAND_SIZE 256

typedef struct MYMSG {
	LPTSTR command;
	HANDLE hPipe;
}MYMSG;
typedef enum DESTINATION { CLIENT, SERVER }DESTINATION;
typedef enum TYPE { ECHO, REGISTER, LOGIN, LOGOUT, LIST, EXIT, MESSAGE, RECEIVED, BROADCAST, HISTORY, SENDFILE, DONE }TYPE;

typedef struct INFOS {
	DESTINATION destination;
	TYPE type;
	TCHAR additional[256];
	TCHAR message[256];
}INFOS;

BOOL loggedIn = FALSE;
DWORD WINAPI ProcessCommand(LPVOID);
DWORD WINAPI ProcessReply(LPVOID);
DWORD WINAPI ProcessSendFile(LPVOID);


int _tmain(int argc, TCHAR *argv[])
{
	HANDLE hPipe;
	HANDLE hHeap = GetProcessHeap();
	LPTSTR command = (LPTSTR)HeapAlloc(hHeap, 0, COMMAND_SIZE * sizeof(TCHAR));//stringul in care se vor pune comenzile
	LPTSTR lpszPipename = TEXT("\\\\.\\pipe\\Proiect2");
	DWORD threadId = 0;
	DWORD dwMode;
	BOOL fSuccess = FALSE;
	if (command == NULL) {
		_tprintf(TEXT("Unexpected error: [Unexpected NULL heap allocation]\n"));
		return -1;
	}

	hPipe = CreateFile(
		lpszPipename,   // pipe name 
		GENERIC_READ |  // read and write access 
		GENERIC_WRITE,
		0,              // no sharing 
		NULL,           // default security attributes
		OPEN_EXISTING,  // opens existing pipe 
		0,              // default attributes 
		NULL);

	if (GetLastError() == ERROR_FILE_NOT_FOUND) {
		_tprintf(TEXT("Error: no running server found\n"));
		return -1;
	}
	if (GetLastError() == ERROR_PIPE_BUSY) {
		_tprintf(TEXT("Error: maximum concurrent connection count reached\n"));
		return -1;
	}
	if (hPipe == INVALID_HANDLE_VALUE) {
		_tprintf(TEXT("Unexpected error: [CreateFile failed, GLE=%d]\n"), GetLastError());
		return -1;
	}
	_tprintf(TEXT("Successful connection\n"));
	dwMode = PIPE_READMODE_MESSAGE;
	fSuccess = SetNamedPipeHandleState(
		hPipe,    // pipe handle 
		&dwMode,  // new pipe mode 
		NULL,     // don't set maximum bytes 
		NULL);    // don't set maximum time 
	if (!fSuccess)
	{
		_tprintf(TEXT("Unexpected error: [SetNamedPipeHandleState failed. GLE=%d]\n"), GetLastError());
		return -1;
	}
	HANDLE hThread1 = CreateThread(NULL, 0, ProcessReply, (LPVOID)hPipe, 0, &threadId);
	CloseHandle(hThread1);
	while (1) {
		while (StringCchGets(command, COMMAND_SIZE * sizeof(TCHAR)) != S_OK) {
			/*_tprintf(TEXT("Unexpected error: [Fetch command failed, GLE=%d]\n"), GetLastError());
			return -1;*/
		}
		MYMSG* msg = (MYMSG*)HeapAlloc(hHeap, 0, sizeof(MYMSG));
		if (msg == NULL) {
			_tprintf(TEXT("Unexpected error: [No more space on Heap]"));
			return -1;
		}
		msg->command = command;
		msg->hPipe = hPipe;
		HANDLE hThread = CreateThread(NULL, 0, ProcessCommand, (LPVOID)msg, 0, &threadId);
		CloseHandle(hThread);
	}

	FlushFileBuffers(hPipe);
	DisconnectNamedPipe(hPipe);
	CloseHandle(hPipe);
	HeapFree(hHeap, 0, command);
	return 0;
}

DWORD WINAPI ProcessCommand(LPVOID lpvParam) {
	MYMSG* msg;
	HANDLE hHeap = GetProcessHeap();
	BOOL fSuccess = FALSE;
	DWORD cbWritten;
	if (lpvParam == NULL) {
		_tprintf(TEXT("Unexpected error: [Instance thread got an unexpected NULL value in lpvParam]\n"));
		return (DWORD)-1;
	}
	msg = (MYMSG*)lpvParam;

	///////////////////////////////////////////////ECHO////////////////////////////
	if (!wcsncmp(msg->command, TEXT("echo"), 4)) {
		INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
		if (NULL == infos) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			return(DWORD)-1;
		}
		infos->destination = SERVER;
		infos->type = ECHO;
		StringCbCopy(infos->message, 256 * sizeof(TCHAR), msg->command + 5);
		fSuccess = WriteFile(msg->hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
		if (!fSuccess)
		{
			HeapFree(hHeap, 0, infos);
			_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
			return -1;
		}
		
		HeapFree(hHeap, 0, infos);
		return (DWORD)1;
	}

	////////////////////////////////////////////REGISTER///////////////////////////////
	else if (!wcsncmp(msg->command, TEXT("register"), 8)) {
		//if user already connected, it can't register
		if (loggedIn == TRUE) {
			_tprintf(TEXT("Error: User already logged in\n"));

			return (DWORD)-1;
		}
		else {
			INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
			if (NULL == infos) {
				_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
				return(DWORD)-1;
			}
			StringCbCopy(infos->message, 255 * sizeof(TCHAR), msg->command + 9);
			LPTSTR user = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
			if (NULL == user) {
				_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
				HeapFree(hHeap, 0, infos);
				return (DWORD)-1;
			}
			LPTSTR pass = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
			if (NULL == pass) {
				_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
				HeapFree(hHeap, 0, infos);
				HeapFree(hHeap, 0, user);
				return (DWORD)-1;
			}

			LPTSTR row;
			LPTSTR rowstate;
			row = wcstok_s(infos->message, TEXT(" "), &rowstate);
			StringCbCopy(user, 255 * sizeof(TCHAR), row);
			row = wcstok_s(NULL, TEXT("\n"), &rowstate);
			StringCbCopy(pass, 255 * sizeof(TCHAR), row);
			/////////user can have only alphanumeric characters
			size_t size;
			StringCchLength(user, 255, &size);
			for (size_t i = 0;i < size;i++) {
				if (user[i] <'A' || (user[i] > 'Z' && user[i] < 'a') || user[i] > 'z') {
					_tprintf(TEXT("Error: Invalid username\n"));
					HeapFree(hHeap, 0, user);
					HeapFree(hHeap, 0, pass);
					HeapFree(hHeap, 0, infos);
					return (DWORD)-1;
				}
			}
			//user is case insensitive
			for (size_t i = 0;i < size;i++) {
				if (*(infos->message + i) >= 'A' && *(infos->message + i) <= 'Z') {
					*(infos->message + i) += 32;//transform it into it's lowercase form
				}
			}
			////////////password verification
			StringCchLength(pass, 255, &size);
			if (size < 5) {
				_tprintf(TEXT("Error: Password too weak\n"));
				HeapFree(hHeap, 0, user);
				HeapFree(hHeap, 0, pass);
				HeapFree(hHeap, 0, infos);
				return (DWORD)-1;
			}
			DWORD up = 0;
			DWORD nonalpha = 0;
			for (size_t i = 0;i < size;i++) {
				if (pass[i] >= 'A' && pass[i] <= 'Z') {
					up++;
				}
				else if (pass[i] < 'A' || (pass[i] > 'Z' && pass[i] < 'a') || pass[i] > 'z') {
					if (pass[i] == ' ' || pass[i] == ',') {
						_tprintf(TEXT("Error: Invalid password\n"));
						HeapFree(hHeap, 0, user);
						HeapFree(hHeap, 0, pass);
						HeapFree(hHeap, 0, infos);
						return (DWORD)-1;
					}
					else {
						nonalpha++;
					}
				}
			}
			if (up == 0 || nonalpha == 0) {
				_tprintf(TEXT("Error: Password too weak\n"));
				HeapFree(hHeap, 0, user);
				HeapFree(hHeap, 0, pass);
				HeapFree(hHeap, 0, infos);
				return (DWORD)-1;
			}

			infos->destination = SERVER;
			infos->type = REGISTER;
			StringCbCopy(infos->message, 255 * sizeof(TCHAR), msg->command + 9);
			fSuccess = WriteFile(msg->hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
			if (!fSuccess)
			{
				_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
				HeapFree(hHeap, 0, user);
				HeapFree(hHeap, 0, pass);
				HeapFree(hHeap, 0, infos);
				return -1;
			}
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, pass);
			HeapFree(hHeap, 0, infos);
			return (DWORD)1;
		}
	}

	///////////////////////////////////////////////LOGIN/////////////////////////////
	else if (!wcsncmp(msg->command, TEXT("login"), 5)) {
		if (loggedIn == TRUE) {
			_tprintf(TEXT("Error: Another user already logged in\n"));
			return (DWORD)-1;
		}
		INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
		if (NULL == infos) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			return(DWORD)-1;
		}
		infos->destination = SERVER;
		infos->type = LOGIN;
		StringCbCopy(infos->message, 255 * sizeof(TCHAR), msg->command + 6);
		int i = 0;
		//case insensitive for username
		while(*(infos->message + i) != ' '){
			if (*(infos->message + i) >= 'A' && *(infos->message + i) <= 'Z') {
				*(infos->message + i) += 32;//transform it into it's lowercase form
			}
			i++;
		}
		fSuccess = WriteFile(msg->hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
		if (!fSuccess)
		{
			_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
			HeapFree(hHeap, 0, infos);
			return -1;
		}
		HeapFree(hHeap, 0, infos);
		return 1;
	}
	//////////////////////////////////////////////////LIST////////////////////////////////
	else if (!wcscmp(msg->command, TEXT("list"))) {
		//doesn't make sense to see the logged in user if you are not logged in
		if (loggedIn == FALSE) {
			_tprintf(TEXT("Error: No user currently logged in\n"));
			return 1;
		}
		INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
		if (NULL == infos) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			return(DWORD)-1;
		}
		infos->destination = SERVER;
		infos->type = LIST;
		StringCbCopy(infos->message, 255 * sizeof(TCHAR), msg->command + 5);
		fSuccess = WriteFile(msg->hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
		if (!fSuccess)
		{
			_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
			HeapFree(hHeap, 0, infos);
			return -1;
		}
		HeapFree(hHeap, 0, infos);
		return 1;
	}
	///////////////////////////////////LOGOUT////////////////////////
	else if (!wcscmp(msg->command, TEXT("logout"))) {
		if (loggedIn == FALSE) {
			_tprintf(TEXT("Error: No user currently logged in\n"));
			return 1;
		}

		INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
		if (NULL == infos) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			return(DWORD)-1;
		}
		infos->destination = SERVER;
		infos->type = LOGOUT;
		fSuccess = WriteFile(msg->hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
		if (!fSuccess)
		{
			_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
			HeapFree(hHeap, 0, infos);
			return -1;
		}
		HeapFree(hHeap, 0, infos);
		return 1;
	}
	////////////////////////////////////////////////EXIT/////////////////////////////////////
	else if (!wcscmp(msg->command, TEXT("exit"))) {
		if (loggedIn == TRUE) {
			INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
			if (NULL == infos) {
				_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
				return(DWORD)-1;
			}
			infos->destination = SERVER;
			infos->type = LOGOUT;
			fSuccess = WriteFile(msg->hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
			if (!fSuccess)
			{
				_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
				HeapFree(hHeap, 0, infos);
				HeapFree(hHeap, 0, infos);
				return -1;
			}
			HeapFree(hHeap, 0, infos);
		}
		exit(0);
	}
	///////////////////////////////////////MSG///////////////////////////
	else if (!wcsncmp(msg->command, TEXT("msg"),3)) {
		if (loggedIn == FALSE) {
			_tprintf(TEXT("Error: No user currently logged in\n"));
			return 1;
		}
		INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
		if (NULL == infos) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			return(DWORD)-1;
		}
		infos->destination = SERVER;
		infos->type = MESSAGE;
		StringCbCopy(infos->message, 256 * sizeof(TCHAR), msg->command + 4);
		fSuccess = WriteFile(msg->hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
		if (!fSuccess)
		{
			_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
			HeapFree(hHeap, 0, infos);
			return -1;
		}
		return 1;
	}
	//////////////////////////////////BROADCAST////////////////////////////////
	else if (!wcsncmp(msg->command, TEXT("broadcast"), 9)) {
		if (loggedIn == FALSE) {
			_tprintf(TEXT("Error: No user currently logged in\n"));
			return 1;
		}
		INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
		if (NULL == infos) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			return(DWORD)-1;
		}
		infos->destination = SERVER;
		infos->type = BROADCAST;
		StringCbCopy(infos->message, 256 * sizeof(TCHAR), msg->command + 10);
		fSuccess = WriteFile(msg->hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
		if (!fSuccess)
		{
			_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
			HeapFree(hHeap, 0, infos);
			return -1;
		}
		return 1;
	}
	/////////////////////////////HISTORY//////////////////////
	else if (!wcsncmp(msg->command, TEXT("history"), 7)) {
		if (loggedIn == FALSE) {
			_tprintf(TEXT("Error: No user currently logged in\n"));
			return 1;
		}
		INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
		if (NULL == infos) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			return(DWORD)-1;
		}
		infos->destination = SERVER;
		infos->type = HISTORY;
		StringCbCopy(infos->message, 256 * sizeof(TCHAR), msg->command + 8);
		fSuccess = WriteFile(msg->hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
		if (!fSuccess)
		{
			_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
			HeapFree(hHeap, 0, infos);
			return -1;
		}
		return 1;
	}
	////////////////////////////SEND FILE///////////////////
	else if (!wcsncmp(msg->command, TEXT("sendfile"), 8)) {
		if (loggedIn == FALSE) {
			_tprintf(TEXT("Error: No user currently logged in\n"));
			return 1;
		}
		DWORD threadId;
		MYMSG* msg1 = (MYMSG*)HeapAlloc(hHeap, 0, sizeof(MYMSG));
		if (msg1 == NULL) {
			_tprintf(TEXT("Unexpected error: [No more space on Heap]"));
			return -1;
		}
		msg1->hPipe = msg->hPipe;
		StringCbCopy(msg1->command, 255 * sizeof(TCHAR), msg->command + 9);
		HANDLE hThread1 = CreateThread(NULL, 0, ProcessSendFile, (LPVOID)msg1, 0, &threadId);
		CloseHandle(hThread1);
		return 1;
	}
	else {
		_tprintf(TEXT("Unexpected error: [Unknown command]\n"));
		return -1;
	}
	return 0;
}
DWORD WINAPI ProcessReply(LPVOID lpvParam) {
	HANDLE hHeap = GetProcessHeap();
	BOOL fSuccess = FALSE;
	if (lpvParam == NULL) {
		_tprintf(TEXT("Unexpected error: [Instance thread got an unexpected NULL value in lpvParam]\n"));
		return (DWORD)-1;
	}
	HANDLE hPipe = (HANDLE)lpvParam;
	DWORD cbRead;
	INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
	if (NULL == infos) {
		_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
		return(DWORD)-1;
	}
	for (;;) {
		do {
			fSuccess = PeekNamedPipe(hPipe, infos, sizeof(INFOS), NULL, NULL, NULL);
			if (!fSuccess && GetLastError() != ERROR_MORE_DATA)
				break;
		} while (!fSuccess);
		if (!fSuccess)
		{
			_tprintf(TEXT("Unexpected error: [PeakNamedPipe failed. GLE=%d]\n"), GetLastError());
			HeapFree(hHeap, 0, infos);
			return -1;
		}
		if (infos->destination == CLIENT) {
			fSuccess = ReadFile(hPipe, infos, sizeof(INFOS), &cbRead, NULL);
			if (!fSuccess)
			{
				_tprintf(TEXT("Unexpected error: [ReadFile from pipe failed. GLE=%d]\n"), GetLastError());
				HeapFree(hHeap, 0, infos);
				return -1;
			}
			if (infos->destination == CLIENT && infos->type == ECHO) {
				_tprintf(TEXT("%s\n"), infos->message);
				infos->destination = SERVER;
			}
			else if (infos->type == REGISTER && infos->destination == CLIENT && !wcscmp(infos->message, TEXT("YES"))) {
				_tprintf(TEXT("Success\n"));
				infos->destination = SERVER;
			}
			else if (infos->type == REGISTER && infos->destination == CLIENT && !wcscmp(infos->message, TEXT("NO"))) {
				_tprintf(TEXT("Error: Username already registered\n"));
				infos->destination = SERVER;
			}
			else if (infos->destination == CLIENT && infos->type == LOGIN && !wcscmp(infos->message, TEXT("invalid"))) {
				_tprintf(TEXT("Error: Invalid username/password combination\n"));
				infos->destination = SERVER;
			}
			else if (infos->destination == CLIENT && infos->type == LOGIN && !wcscmp(infos->message, TEXT("logged"))) {
				_tprintf(TEXT("Error: User already logged in\n"));
				infos->destination = SERVER;
			}
			else if (infos->destination == CLIENT && infos->type == LOGIN && !wcscmp(infos->message, TEXT("YES"))) {
				_tprintf(TEXT("Success\n"));
				loggedIn = TRUE;
				infos->destination = SERVER;
			}
			else if (infos->destination == CLIENT && infos->type == LIST) {
				_tprintf(TEXT("%s\n"), infos->message);
				infos->destination = SERVER;
			}
			else if (infos->type == LOGOUT && infos->destination == CLIENT && !wcscmp(infos->message, TEXT("NO"))) {
				_tprintf(TEXT("Unexpected error: [Something went wrong]\n"));
				infos->destination = SERVER;
			}
			else if (infos->type == LOGOUT && infos->destination == CLIENT && !wcscmp(infos->message, TEXT("YES"))) {
				_tprintf(TEXT("Success\n"));
				loggedIn = FALSE;
				infos->destination = SERVER;
			}
			else if (infos->type == MESSAGE && infos->destination == CLIENT && !wcscmp(infos->message, TEXT("NO"))) {
				_tprintf(TEXT("Error: No such user\n"));
				infos->destination = SERVER;
			}
			else if (infos->type == MESSAGE && infos->destination == CLIENT && !wcscmp(infos->message, TEXT("YES"))) {
				_tprintf(TEXT("Success\n"));
				infos->destination = SERVER;
			}
			else if (infos->type == RECEIVED && infos->destination == CLIENT) {
				_tprintf(TEXT("%s\n"),infos->message);
				infos->destination = SERVER;
			}
			else if (infos->type == BROADCAST && infos->destination == CLIENT) {
				_tprintf(TEXT("Success\n"));
				infos->destination = SERVER;
			}
			else if (infos->type == HISTORY && infos->destination == CLIENT && !wcscmp(infos->message, TEXT("INVALID"))) {
				_tprintf(TEXT("Error: Command format illegal\n"));
				infos->destination = SERVER;
			}
			else if (infos->type == HISTORY && infos->destination == CLIENT && !wcscmp(infos->message, TEXT("NO"))) {
				_tprintf(TEXT("Error: No such user\n"));
				infos->destination = SERVER;
			}
			else if (infos->type == HISTORY && infos->destination == CLIENT && !wcscmp(infos->message, TEXT("DONE"))) {
				infos->destination = SERVER;
			}
			else if (infos->type == HISTORY && infos->destination == CLIENT && !wcscmp(infos->message, TEXT("\n"))) {
				_tprintf(TEXT("\n"));
				infos->destination = SERVER;
			}
			else if (infos->type == DONE && infos->destination == CLIENT) {
				_tprintf(TEXT("Success\n"));
				infos->destination = SERVER;
			}
			else if (infos->type == SENDFILE && infos->destination == CLIENT && !wcscmp(infos->message, TEXT("NO"))) {
				_tprintf(TEXT("Error: User not active\n"));
				infos->destination = SERVER;
			}
			else if (infos->type == SENDFILE && infos->destination == CLIENT) {
				//in infos->additional we have the name of the file that is sent
				//first, we decode the path where the exe is located
				LPTSTR filePath = (LPTSTR)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
				if (NULL == filePath) {
					_tprintf(TEXT("Unexpected error: [No more space on Heap]\n"));
					return (DWORD)-1;
				}
				GetModuleFileName(NULL, filePath, 255 * sizeof(TCHAR));
				DWORD sizeOfPath;
				StringCchLength(filePath, 255, &sizeOfPath);
				LPTSTR filePath1 = (LPTSTR)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
				if (NULL == filePath1) {
					_tprintf(TEXT("Unexpected error: [No more space on Heap]\n"));
					HeapFree(hHeap, 0, filePath);
					return (DWORD)-1;
				}
				StringCchCopy(filePath1, sizeOfPath - 9, filePath);
				StringCbCat(filePath1, 255 * sizeof(TCHAR), infos->additional);
				HeapFree(hHeap, 0, filePath);
				//now, we either create or open( if is not the first iteration) the file
				HANDLE hFile1 = CreateFile(filePath1, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
				if (hFile1 == INVALID_HANDLE_VALUE) {
					_tprintf(TEXT("Unexpected error[Cannot create/open file. Error: %x]\n"), GetLastError());
					CloseHandle(hFile1);
					HeapFree(hHeap, 0, filePath1);
					return -1;
				}
				SetFilePointer(hFile1, 0, NULL, FILE_END);
				WriteFile(hFile1, infos->message, 256 * sizeof(TCHAR), NULL, NULL);
				CloseHandle(hFile1);
				HeapFree(hHeap, 0, filePath1);
				infos->destination = SERVER;

			}
			else {
				_tprintf(TEXT("Unexpected error: [Command not found]\n"));
			}
		}
	}
	HeapFree(hHeap, 0, infos);
	return 1;
}
DWORD WINAPI ProcessSendFile(LPVOID lpvParam) {
	MYMSG* msg;
	HANDLE hHeap = GetProcessHeap();
	BOOL fSuccess = FALSE;
	if (lpvParam == NULL) {
		_tprintf(TEXT("Unexpected error: [Instance thread got an unexpected NULL value in lpvParam]\n"));
		return (DWORD)-1;
	}
	INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
	if (NULL == infos) {
		_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
		return(DWORD)-1;
	}
	msg = (MYMSG*)lpvParam;
	LPTSTR buffer = (LPTSTR)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
	if (NULL == buffer) {
		_tprintf(TEXT("Unexpected error: [No more space on Heap]\n"));
		HeapFree(hHeap, 0, infos);
		return (DWORD)-1;
	}
	StringCbCopy(buffer, 255 * sizeof(TCHAR), msg->command);
	LPTSTR row;
	LPTSTR rowstate;
	row = wcstok_s(msg->command, TEXT(" "), &rowstate);
	
	LPTSTR filePath = (LPTSTR)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
	if (NULL == filePath) {
		_tprintf(TEXT("Unexpected error: [No more space on Heap]\n"));
		HeapFree(hHeap, 0, buffer);
		HeapFree(hHeap, 0, infos);
		return (DWORD)-1;
	}
	GetModuleFileName(NULL, filePath, 255 * sizeof(TCHAR));
	DWORD sizeOfPath;
	StringCchLength(filePath, 255, &sizeOfPath);
	LPTSTR filePath1 = (LPTSTR)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
	if (NULL == filePath1) {
		_tprintf(TEXT("Unexpected error: [No more space on Heap]\n"));
		HeapFree(hHeap, 0, buffer);
		HeapFree(hHeap, 0, infos);
		HeapFree(hHeap, 0, filePath);
		return (DWORD)-1;
	}
	StringCchCopy(filePath1, sizeOfPath - 9, filePath);
	StringCbCat(filePath1, 255 * sizeof(TCHAR), rowstate);
	HeapFree(hHeap, 0, filePath);
	
	HANDLE hFile;
	hFile = CreateFile(filePath1, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE) {
		_tprintf(TEXT("Error: File not found"));
		HeapFree(hHeap, 0, infos);
		HeapFree(hHeap, 0, buffer);
		HeapFree(hHeap, 0, filePath1);
		return -1;
	}
	DWORD nIn,cbWritten;
	infos->type = SENDFILE;
	infos->destination = SERVER;
	StringCbCopy(infos->additional, 255 * sizeof(TCHAR), buffer);
	while (ReadFile(hFile, infos->message, 255*sizeof(TCHAR), &nIn, NULL) && nIn > 0) {
		fSuccess = WriteFile(msg->hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
		if (!fSuccess)
		{
			_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
			HeapFree(hHeap, 0, buffer);
			HeapFree(hHeap, 0, filePath1);
			HeapFree(hHeap, 0, infos);
			CloseHandle(hFile);
			return -1;
		}
		Sleep(100);//to ensure that the file will be send in the correct order
	}
	//when the transmition is over, we send a message to tell that to the server, and then to the sender
	infos->destination = SERVER;
	infos->type = DONE;
	fSuccess = WriteFile(msg->hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
	if (!fSuccess)
	{
		_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
		HeapFree(hHeap, 0, buffer);
		HeapFree(hHeap, 0, filePath1);
		HeapFree(hHeap, 0, infos);
		CloseHandle(hFile);
		return -1;
	}
	HeapFree(hHeap, 0, buffer);
	HeapFree(hHeap, 0, filePath1);
	HeapFree(hHeap, 0, infos);
	CloseHandle(hFile);
	return 1;
}

