
#include "stdafx.h"
#include "constants.h"
#include <Windows.h>
#include <stdio.h>
#include <tchar.h>
#include <strsafe.h>
#include <wchar.h>


#define BUFSIZE 1024
//for registration

typedef struct ACCOUNT {
	LPTSTR user;
	LPTSTR password;
}ACCOUNT;

typedef struct ACTIVE_ACCOUNT {
	LPTSTR user;
	HANDLE hPipe;
}ACTIVE_ACCOUNT;

typedef enum DESTINATION { CLIENT, SERVER }DESTINATION;
typedef enum TYPE { ECHO, REGISTER, LOGIN, LOGOUT, LIST, EXIT, MESSAGE, RECEIVED, BROADCAST, HISTORY, SENDFILE, DONE }TYPE;

typedef struct INFOS {
	DESTINATION destination;
	TYPE type;
	TCHAR additional[256];
	TCHAR message[256];
}INFOS;

volatile ACCOUNT* accounts;
volatile DWORD numOfAcc = 0;
volatile DWORD accountSize = 100;

volatile ACTIVE_ACCOUNT* activeAccounts;
volatile DWORD numOfActiveAcc = 0;
volatile DWORD activeAccountSize = 100;

CRITICAL_SECTION activeAccountsLock;
CRITICAL_SECTION accountsLock;
CRITICAL_SECTION regLock;

DWORD WINAPI InstanceThread(LPVOID);//threadul ce se ocupa de instante
VOID GetAnswerToRequest(INFOS*, INFOS*, HANDLE);//functia ce se ocupa de procesarea 
VOID GetRegisteredClients();
int _tmain(int argc, TCHAR *argv[])
{
	BOOL   fConnected = FALSE;
	DWORD  dwThreadId = 0;
	HANDLE hPipe = INVALID_HANDLE_VALUE, hThread = NULL;
	LPTSTR lpszPipename = TEXT("\\\\.\\pipe\\Proiect2");
	DWORD maxClients = _wtoi(argv[1]);
	if (!maxClients) {
		_tprintf(TEXT("Error: invalid maximum number of connections\n"));
		return -1;
	}
	_tprintf(TEXT("Succes\n"));
	InitializeCriticalSection(&activeAccountsLock);
	InitializeCriticalSection(&accountsLock);
	InitializeCriticalSection(&regLock);
	GetRegisteredClients();

	for (;;) {
		hPipe = CreateNamedPipe(
			lpszPipename,             // pipe name 
			PIPE_ACCESS_DUPLEX,       // read/write access 
			PIPE_TYPE_MESSAGE |       // message type pipe 
			PIPE_READMODE_MESSAGE |   // message-read mode 
			PIPE_WAIT,                // blocking mode 
			maxClients,			// max. instances  
			BUFSIZE * sizeof(TCHAR),                  // output buffer size 
			BUFSIZE * sizeof(TCHAR),                  // input buffer size 
			0,                        // client time-out 
			NULL);
		if (hPipe == INVALID_HANDLE_VALUE) {
			_tprintf(TEXT("Unexpected error: [CreateNamedPipe failed, GLE=%d]\n"), GetLastError());
			return -1;
		}
		fConnected = ConnectNamedPipe(hPipe, NULL) ?
			TRUE : (GetLastError() == ERROR_PIPE_CONNECTED);
		if (fConnected)
		{
			// Create a thread for this client. 
			hThread = CreateThread(
				NULL,              // no security attribute 
				0,                 // default stack size 
				InstanceThread,    // thread proc
				(LPVOID)hPipe,    // thread parameter 
				0,                 // not suspended 
				&dwThreadId);      // returns thread ID 

			if (hThread == NULL)
			{
				_tprintf(TEXT("Unexpected error: [CreateThread failed, GLE=%d]\n"), GetLastError());
				return -1;
			}
			else {
				CloseHandle(hThread);
			}
		}
		else {
			CloseHandle(hPipe);
		}

	}
	HANDLE hHeap = GetProcessHeap();
	for (int i = 0;i < numOfAcc;i++) {
		HeapFree(hHeap, 0, (accounts + i)->user);
		HeapFree(hHeap, 0, (accounts + i)->password);
	}
	for (int i = 0;i < numOfActiveAcc;i++) {
		HeapFree(hHeap, 0, (activeAccounts + i)->user);
	}
	HeapFree(hHeap, 0, activeAccounts);
	HeapFree(hHeap, 0, accounts);
	DeleteCriticalSection(&activeAccountsLock);
	DeleteCriticalSection(&accountsLock);
	DeleteCriticalSection(&regLock);

	return 0;
}
DWORD WINAPI InstanceThread(LPVOID lpvParam) {
	HANDLE hHeap = GetProcessHeap();
	INFOS* pchRequest = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
	INFOS* pchReply = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
	DWORD cbBytesRead, cbWritten;
	BOOL fSuccess = FALSE;
	HANDLE hPipe = NULL;

	if (lpvParam == NULL) {
		_tprintf("Unexpected error: [Instance thread got an unexpected NULL value in lpvParam]\n");
		if (pchReply != NULL) {
			HeapFree(hHeap, 0, pchReply);
		}
		if (pchRequest != NULL) {
			HeapFree(hHeap, 0, pchRequest);
		}
		return (DWORD)-1;
	}
	if (pchRequest == NULL) {
		_tprintf("Unexpected error: [Instance thread got an unexpected NULL heap allocation]\n");
		if (pchReply != NULL) {
			HeapFree(hHeap, 0, pchReply);
		}
		return (DWORD)-1;
	}
	if (pchReply == NULL) {
		_tprintf("Unexpected error: [Instance thread got an unexpected NULL heap allocation]\n");
		if (pchRequest != NULL) {
			HeapFree(hHeap, 0, pchRequest);
		}
		return (DWORD)-1;
	}
	hPipe = (HANDLE)lpvParam;
	while (1) {
		fSuccess = PeekNamedPipe(hPipe, pchRequest, sizeof(INFOS), NULL, NULL, NULL);
		if (!fSuccess)
		{
			if (GetLastError() == ERROR_BROKEN_PIPE)
			{
				_tprintf(TEXT("Unexpected error: [client disconnected]\n"), GetLastError());
				//if he disconnects, we verify if he was logged in, so we could log him out
				INT pos = -1;
				EnterCriticalSection(&activeAccountsLock);
				for (int i = 0;i < numOfActiveAcc;i++) {
					if ((activeAccounts + i)->hPipe == hPipe) {
						pos = i;
						break;
					}
				}
				LeaveCriticalSection(&activeAccountsLock);

				if (pos >= 0) {
					EnterCriticalSection(&activeAccountsLock);
					if (pos == numOfActiveAcc - 1) {
						HeapFree(hHeap, 0, (activeAccounts + pos)->user);
						numOfActiveAcc--;
					}
					else {
						for (DWORD i = pos;i < numOfActiveAcc - 1;i++) {
							StringCchCopy((activeAccounts + i)->user, 255 * sizeof(TCHAR), (activeAccounts + i + 1)->user);
							(activeAccounts + i)->hPipe = (activeAccounts + i + 1)->hPipe;
						}
						numOfActiveAcc--;
					}
					LeaveCriticalSection(&activeAccountsLock);
				}
			}
			else
			{
				_tprintf(TEXT("Unexpected error: [PeekNamedPipe failed, GLE=%d]\n"), GetLastError());
			}
			HeapFree(hHeap, 0, pchReply);
			HeapFree(hHeap, 0, pchRequest);
			return -1;
		}
		if (pchRequest->destination == SERVER) {
			fSuccess = ReadFile(
				hPipe,        // handle to pipe 
				pchRequest,    // buffer to receive data 
				sizeof(INFOS), // size of buffer 
				&cbBytesRead, // number of bytes read 
				NULL);        // not overlapped I/O 
			if (!fSuccess || cbBytesRead == 0)
			{
				if (GetLastError() == ERROR_BROKEN_PIPE)
				{
					_tprintf(TEXT("Unexpected error: [client disconnected]\n"), GetLastError());
				}
				else
				{
					_tprintf(TEXT("Unexpected error: [ReadFile failed, GLE=%d]\n"), GetLastError());
				}
				break;
			}
			GetAnswerToRequest(pchRequest, pchReply, hPipe);
			pchRequest->destination = CLIENT;
			fSuccess = WriteFile(
				hPipe,        // handle to pipe 
				pchReply,     // buffer to write from 
				sizeof(INFOS), // number of bytes to write 
				&cbWritten,   // number of bytes written 
				NULL);
			if (!fSuccess || sizeof(INFOS) != cbWritten)
			{
				_tprintf(TEXT("Unexpected error: [WriteFile failed, GLE=%d]\n"), GetLastError());
				break;
			}
		}
	}
	FlushFileBuffers(hPipe);
	DisconnectNamedPipe(hPipe);
	CloseHandle(hPipe);

	HeapFree(hHeap, 0, pchRequest);
	HeapFree(hHeap, 0, pchReply);

	return (DWORD)1;
}
VOID GetAnswerToRequest(INFOS* pchRequest, INFOS* pchReply, HANDLE hPipe) {
	HANDLE hHeap = GetProcessHeap();
	///////////////////////////////////ECHO////////////////////////////////
	if (pchRequest->type == ECHO) {
		_tprintf(TEXT("%s\n"), pchRequest->message);
		*pchReply = *pchRequest;
		pchReply->destination = CLIENT;
		return;
	}
	
	///////////////////////////////////REGISTER//////////////////////////
	else if (pchRequest->type == REGISTER) {
		LPTSTR user = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == user) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			return;
		}
		LPTSTR pass = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == pass) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			return;
		}
		LPTSTR row;
		LPTSTR rowstate;
		row = wcstok_s(pchRequest->message, TEXT(" "), &rowstate);
		StringCbCopy(user, 255 * sizeof(TCHAR), row);
		row = wcstok_s(NULL, TEXT("\n"), &rowstate);
		StringCbCopy(pass, 255 * sizeof(TCHAR), row);
		//verify if user already exists
		EnterCriticalSection(&accountsLock);
		for (int i = 0;i < numOfAcc;i++) {
			if (!wcscmp(user, (accounts + i)->user)) {
				StringCchCopy(pchReply->message, 3 * sizeof(TCHAR), TEXT("NO"));
				pchReply->destination = CLIENT;
				pchReply->type = REGISTER;
				HeapFree(hHeap, 0, user);
				HeapFree(hHeap, 0, pass);
				LeaveCriticalSection(&accountsLock);
				return;
			}
		}
		LeaveCriticalSection(&accountsLock);
		//we create 2 files for user, userIstoric.txt and userMesaje.txt(here we hold the messages he received offline)
		LPTSTR name = (LPTSTR)HeapAlloc(hHeap, 0, 256 * sizeof(TCHAR));
		if (NULL == name) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, pass);
			return;
		}
		StringCbCopy(name, 256 * sizeof(TCHAR), user);
		StringCbCat(name, 256 * sizeof(TCHAR), TEXT("Mesaje.txt"));
		HANDLE hMFile = CreateFile(name, GENERIC_READ, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hMFile == INVALID_HANDLE_VALUE) {
			_tprintf(TEXT("Unexpected error[Cannot create file. Error: %x]\n"), GetLastError());
			CloseHandle(hMFile);
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, pass);
			HeapFree(hHeap, 0, name);
			return;
		}
		StringCbCopy(name, 256 * sizeof(TCHAR), user);
		StringCbCat(name, 256 * sizeof(TCHAR), TEXT("Istoric.txt"));
		HANDLE hHFile = CreateFile(name, GENERIC_READ, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hHFile == INVALID_HANDLE_VALUE) {
			_tprintf(TEXT("Unexpected error[Cannot create file. Error: %x]\n"), GetLastError());
			CloseHandle(hMFile);
			CloseHandle(hHFile);
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, pass);
			HeapFree(hHeap, 0, name);
			return;
		}
		//we add the account so he can login after, in the same session
		EnterCriticalSection(&accountsLock);
		(accounts + numOfAcc)->user = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == (accounts + numOfAcc)->user) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, pass);
			HeapFree(hHeap, 0, name);
			CloseHandle(hMFile);
			CloseHandle(hHFile);
			LeaveCriticalSection(&accountsLock);
			return;
		}
		StringCbCopy((accounts + numOfAcc)->user, 256 * sizeof(TCHAR), user);
		(accounts + numOfAcc)->password = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == (accounts + numOfAcc)->password) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, pass);
			HeapFree(hHeap, 0, name);
			CloseHandle(hMFile);
			CloseHandle(hHFile);
			LeaveCriticalSection(&accountsLock);
			return;
		}
		StringCbCopy((accounts + numOfAcc)->password, 256 * sizeof(TCHAR), pass);
		numOfAcc++;
		LeaveCriticalSection(&accountsLock);
		//in the end, we put the user in registration.txt
		HANDLE hFile;
		hFile = CreateFile(TEXT("C:\\registration.txt"),
			GENERIC_WRITE,
			0,
			NULL,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			NULL);
		if (hFile == INVALID_HANDLE_VALUE) {
			_tprintf(TEXT("Unexpected error[Cannot open registration file. Error: %x]\n"), GetLastError());
			CloseHandle(hFile);
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, pass);
			HeapFree(hHeap, 0, name);
			CloseHandle(hMFile);
			CloseHandle(hHFile);
			return;
		}
		StringCchCat(user, 255 * sizeof(TCHAR), TEXT(","));
		StringCchCat(pass, 255 * sizeof(TCHAR), TEXT("\r\n"));
		StringCchCat(user, 255 * sizeof(TCHAR), pass);
		DWORD cbToWrite = lstrlen(user) * sizeof(TCHAR);
		DWORD nout;

		EnterCriticalSection(&regLock);
		SetFilePointer(hFile, 0, NULL, FILE_END);
		WriteFile(hFile, user, cbToWrite, &nout, NULL);
		LeaveCriticalSection(&regLock);

		if (cbToWrite != nout) {
			_tprintf(TEXT("Unexpected error[Cannot write in registration file. Error: %x]\n"), GetLastError());
			CloseHandle(hFile);
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, pass);
			HeapFree(hHeap, 0, name);
			CloseHandle(hMFile);
			CloseHandle(hHFile);
			return;
		}

		StringCchCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("YES"));
		pchReply->destination = CLIENT;
		pchReply->type = REGISTER;
		CloseHandle(hFile);
		HeapFree(hHeap, 0, user);
		HeapFree(hHeap, 0, pass);
		HeapFree(hHeap, 0, name);
		CloseHandle(hMFile);
		CloseHandle(hHFile);
		return;
	}
	
	//////////////////////////////////////////////LOGIN///////////////////////////////////////
	else if (pchRequest->type == LOGIN) {
		LPTSTR user = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == user) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			return;
		}
		LPTSTR pass = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == pass) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			return;
		}
		LPTSTR row;
		LPTSTR rowstate;
		row = wcstok_s(pchRequest->message, TEXT(" "), &rowstate);
		StringCbCopy(user, 255 * sizeof(TCHAR), row);
		row = wcstok_s(NULL, TEXT("\n"), &rowstate);
		StringCbCopy(pass, 255 * sizeof(TCHAR), row);
		//verify user and password combination
		BOOL valid = FALSE;
		EnterCriticalSection(&accountsLock);
		for (int i = 0;i < numOfAcc;i++) {
		
			if (!wcscmp((accounts + i)->user, user) && !wcscmp((accounts + i)->password, pass)) {
				valid = TRUE;
			}
			if (valid == TRUE) {
				break;
			}
		}
		LeaveCriticalSection(&accountsLock);
		//invalid user/pass combination
		if (valid == FALSE) {
			StringCbCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("invalid"));
			pchReply->type = LOGIN;
			pchReply->destination = CLIENT;
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, pass);
			return;
		}
		//otherwise, we search to see if the user is already logged in elsewhere
		EnterCriticalSection(&activeAccountsLock);
		for (int i = 0;i < numOfActiveAcc;i++) {
			if (!wcscmp((activeAccounts + i)->user, user)) {
				//in this case, the user is already logged in
				StringCbCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("logged"));
				pchReply->type = LOGIN;
				pchReply->destination = CLIENT;
				HeapFree(hHeap, 0, user);
				HeapFree(hHeap, 0, pass);
				LeaveCriticalSection(&activeAccountsLock);
				return;
			}
		}
		LeaveCriticalSection(&activeAccountsLock);
		//if we reach this line, it means that the user is valid and not logged in somewhere else
		//we search for the offline messages received
		LPTSTR filePath = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == filePath) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, pass);
			return;
		}
		StringCbCopy(filePath, 255 * sizeof(TCHAR), user);
		StringCbCat(filePath, 255 * sizeof(TCHAR), TEXT("Mesaje.txt"));
		HANDLE hFile = CreateFile(filePath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile == INVALID_HANDLE_VALUE) {
			_tprintf(TEXT("Unexpected error[Can not open message file. Error: %x]\n"), GetLastError());
			CloseHandle(hFile);
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, pass);
			HeapFree(hHeap, 0, filePath);
			return;
		}
		INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
		if (NULL == infos) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			CloseHandle(hFile);
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, pass);
			HeapFree(hHeap, 0, filePath);
			return;
		}
		infos->destination = CLIENT;
		infos->type = RECEIVED;
		DWORD nIn,cbWritten;
		BOOL fSuccess = FALSE;
		while (ReadFile(hFile, infos->message, 250 * sizeof(TCHAR), &nIn, NULL) && nIn > 0) {
			fSuccess = WriteFile(hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
			if (!fSuccess)
			{
				_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
				CloseHandle(hFile);
				HeapFree(hHeap, 0, user);
				HeapFree(hHeap, 0, pass);
				HeapFree(hHeap, 0, filePath);
				return;
			}
		}
		CloseHandle(hFile);
		//we clear the content of the file
		HANDLE hFile2 = CreateFile(filePath, GENERIC_READ, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile2 == INVALID_HANDLE_VALUE) {
			_tprintf(TEXT("Unexpected error[Can not open message file. Error: %x]\n"), GetLastError());
			CloseHandle(hFile2);
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, pass);
			HeapFree(hHeap, 0, filePath);
			return;
		}
		CloseHandle(hFile2);
		HeapFree(hHeap, 0, filePath);
		HeapFree(hHeap, 0, infos);
		EnterCriticalSection(&activeAccountsLock);
		(activeAccounts + numOfActiveAcc)->user = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == (activeAccounts + numOfActiveAcc)->user) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, pass);
			LeaveCriticalSection(&activeAccountsLock);
			return;
		}
		StringCbCopy((activeAccounts + numOfActiveAcc)->user, 255 * sizeof(TCHAR), user);
		(activeAccounts + numOfActiveAcc)->hPipe = hPipe;
		numOfActiveAcc++;
		if (numOfActiveAcc == activeAccountSize - 1) {
			ACTIVE_ACCOUNT* temp = (ACTIVE_ACCOUNT*)HeapAlloc(hHeap, 0, activeAccountSize * 2 * sizeof(ACTIVE_ACCOUNT));
			if (temp == NULL) {
				_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
				HeapFree(hHeap, 0, user);
				HeapFree(hHeap, 0, pass);
				LeaveCriticalSection(&activeAccountsLock);
				return;
			}
			for (int i = 0;i < activeAccountSize;i++) {
				(temp + i)->user = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
				if (NULL == (temp + i)->user) {
					_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
					HeapFree(hHeap, 0, user);
					HeapFree(hHeap, 0, pass);
					LeaveCriticalSection(&activeAccountsLock);
					return;
				}
				StringCbCopy((temp + i)->user, 255 * sizeof(TCHAR), (activeAccounts + i)->user);
				(temp + i)->hPipe = (activeAccounts + i)->hPipe;
			}
			HeapFree(hHeap, 0, activeAccounts);
			activeAccounts = temp;
			temp = NULL;
			activeAccountSize *= 2;
		}
		LeaveCriticalSection(&activeAccountsLock);

		StringCbCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("YES"));
		pchReply->type = LOGIN;
		pchReply->destination = CLIENT;
		HeapFree(hHeap, 0, user);
		HeapFree(hHeap, 0, pass);
		return;
	}
	
	////////////////////////////////////////////////LIST///////////////////////////////////////
	else if (pchRequest->type == LIST) {
		StringCbCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT(""));
		EnterCriticalSection(&activeAccountsLock);
		for (int i = 0;i < numOfActiveAcc;i++) {
			StringCchCat(pchReply->message, 255 * sizeof(TCHAR), (activeAccounts + i)->user);
			StringCchCat(pchReply->message, 255 * sizeof(TCHAR), TEXT("\r\n"));
		}
		LeaveCriticalSection(&activeAccountsLock);

		pchReply->destination = CLIENT;
		pchReply->type = LIST;
		return;
	}
	
	////////////////////////////////////////////////LOGOUT//////////////////////////////////////
	else if (pchRequest->type == LOGOUT) {
		//we search in the active user structure for the one logged in on this connection
		DWORD pos = -1;
		EnterCriticalSection(&activeAccountsLock);
		for (int i = 0;i < numOfActiveAcc;i++) {
			if ((activeAccounts + i)->hPipe == hPipe) {
				pos = i;
				break;
			}
		}
		LeaveCriticalSection(&activeAccountsLock);

		if (pos == -1) {
			StringCchCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("NO"));
			pchReply->destination = CLIENT;
			pchReply->type = LOGOUT;
			return;
		}
		//if all goes ok, we delete the corresponding logged in user
		EnterCriticalSection(&activeAccountsLock);
		if (pos == numOfActiveAcc - 1) {
			numOfActiveAcc--;
			StringCchCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("YES"));
			pchReply->destination = CLIENT;
			pchReply->type = LOGOUT;
			LeaveCriticalSection(&activeAccountsLock);
			return;
		}
		for (DWORD i = pos;i < numOfActiveAcc - 1;i++) {
			StringCchCopy((activeAccounts + i)->user, 255 * sizeof(TCHAR), (activeAccounts + i + 1)->user);
			(activeAccounts + i)->hPipe = (activeAccounts + i + 1)->hPipe;
		}
		numOfActiveAcc--;
		LeaveCriticalSection(&activeAccountsLock);

		StringCchCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("YES"));
		pchReply->destination = CLIENT;
		pchReply->type = LOGOUT;
		return;
	}
	///////////////////////////////MSG///////////////////////////////
	else if (pchRequest->type == MESSAGE) {
		LPTSTR user = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == user) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			return;
		}
		LPTSTR msg = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == msg) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			return;
		}
		LPTSTR row;
		LPTSTR rowstate;
		row = wcstok_s(pchRequest->message, TEXT(" "), &rowstate);
		StringCbCopy(user, 255 * sizeof(TCHAR), row);
		row = wcstok_s(NULL, TEXT("\n"), &rowstate);
		StringCbCopy(msg, 255 * sizeof(TCHAR), row);
		//check if the destinatar exists
		BOOL exists = FALSE;
		EnterCriticalSection(&accountsLock);
		for (int i = 0;i < numOfAcc;i++) {
			if (!wcscmp(user, (accounts + i)->user)) {
				exists = TRUE;
				break;
			}
		}
		LeaveCriticalSection(&accountsLock);
		if (exists == FALSE) {
			StringCchCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("NO"));
			pchReply->destination = CLIENT;
			pchReply->type = MESSAGE;
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, msg);
			return;
		}
		
		//get the sender
		LPTSTR from = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == from) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, msg);
			return;
		}
		exists = FALSE;
		//check if the destinatar is on-line
		EnterCriticalSection(&activeAccountsLock);
		for (int i = 0;i < numOfActiveAcc;i++) {
			if (hPipe == (activeAccounts + i)->hPipe) {
				StringCbCopy(from, 255 * sizeof(TCHAR), (activeAccounts + i)->user);
			}
		}
		for (int i = 0;i < numOfActiveAcc;i++) {
			if (!wcscmp(user, (activeAccounts + i)->user)) {
				//if so, send the message to the user
				exists = TRUE;
				BOOL fSuccess = FALSE;
				DWORD cbWritten;
				INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
				if (NULL == infos) {
					_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
					HeapFree(hHeap, 0, user);
					HeapFree(hHeap, 0, msg);
					HeapFree(hHeap, 0, from);
					HeapFree(hHeap, 0, infos);
					LeaveCriticalSection(&activeAccountsLock);
					return;
				}
				infos->destination = CLIENT;
				infos->type = RECEIVED;
				StringCbCopy(infos->message, 256 * sizeof(TCHAR), TEXT("Message from "));
				StringCbCat(infos->message, 256 * sizeof(TCHAR), from);
				StringCbCat(infos->message, 256 * sizeof(TCHAR), TEXT(": "));
				StringCbCat(infos->message, 256 * sizeof(TCHAR), msg);
				StringCbCat(infos->message, 256 * sizeof(TCHAR), TEXT("\r\n"));
				fSuccess = WriteFile((activeAccounts + i)->hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
				if (!fSuccess)
				{
					_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
					HeapFree(hHeap, 0, user);
					HeapFree(hHeap, 0, msg);
					HeapFree(hHeap, 0, from);
					HeapFree(hHeap, 0, infos);
					LeaveCriticalSection(&activeAccountsLock);
					return;
				}
				HeapFree(hHeap, 0, infos);
			}
		}
		LeaveCriticalSection(&activeAccountsLock);
		LPTSTR filePath = (LPTSTR)HeapAlloc(hHeap, 0, 256 * sizeof(TCHAR));
		if (NULL == filePath) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, msg);
			HeapFree(hHeap, 0, from);
			return;
		}
		LPTSTR message = (LPTSTR)HeapAlloc(hHeap, 0, 256 * sizeof(TCHAR));
		if (NULL == message) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, msg);
			HeapFree(hHeap, 0, from);
			HeapFree(hHeap, 0, filePath);
			return;
		}
		DWORD sizeOfBuffer;

		if (TRUE == exists) {
			goto istorie;
			//even if the destinatar is online, we have to put the message in both istorc files
		}
		//if not, we put the message in destinatar's file
		
		StringCbCopy(filePath, 256 * sizeof(TCHAR), user);
		StringCbCat(filePath, 256 * sizeof(TCHAR), TEXT("Mesaje.txt"));
		HANDLE hFile = CreateFile(filePath, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile == INVALID_HANDLE_VALUE) {
			_tprintf(TEXT("Unexpected error[Can not open message file. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, msg);
			HeapFree(hHeap, 0, from);
			HeapFree(hHeap, 0, filePath);
			HeapFree(hHeap, 0, message);
		}
		
		SetFilePointer(hFile, 0, NULL, FILE_END);
		StringCbCopy(message, 256 * sizeof(TCHAR), TEXT("Message from "));
		StringCbCat(message, 256 * sizeof(TCHAR), from);
		StringCbCat(message, 256 * sizeof(TCHAR), TEXT(": "));
		StringCbCat(message, 256 * sizeof(TCHAR), msg);
		StringCbCat(message, 256 * sizeof(TCHAR), TEXT("\r\n"));
		BOOL fSuccess = FALSE;
		DWORD cbWrite;
		StringCbLength(message, 256 * sizeof(TCHAR), &sizeOfBuffer);
		fSuccess = WriteFile(hFile, message, sizeOfBuffer, &cbWrite, NULL);
		if (!fSuccess) {
			_tprintf(TEXT("Unexpected error[Write to message file failed. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, msg);
			HeapFree(hHeap, 0, from);
			HeapFree(hHeap, 0, filePath);
			HeapFree(hHeap, 0, message);
			CloseHandle(hFile);
			return;
		}
		CloseHandle(hFile);
		//and we put the message in the history files
	istorie:
		StringCbCopy(filePath, 256 * sizeof(TCHAR), user);
		StringCbCat(filePath, 256 * sizeof(TCHAR), TEXT("Istoric.txt"));
		StringCbCopy(message, 256 * sizeof(TCHAR), TEXT("From "));
		StringCbCat(message, 256 * sizeof(TCHAR), from);
		StringCbCat(message, 256 * sizeof(TCHAR), TEXT(": "));
		StringCbCat(message, 256 * sizeof(TCHAR), msg);
		StringCbCat(message, 256 * sizeof(TCHAR), TEXT("\r\n"));
		HANDLE hFile1 = CreateFile(filePath, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile1 == INVALID_HANDLE_VALUE) {
			_tprintf(TEXT("Unexpected error[Can not open history file. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, msg);
			HeapFree(hHeap, 0, from);
			HeapFree(hHeap, 0, filePath);
			HeapFree(hHeap, 0, message);
			return;
		}
		SetFilePointer(hFile1, 0, NULL, FILE_END);
		StringCbLength(message, 256 * sizeof(TCHAR), &sizeOfBuffer);
		fSuccess = WriteFile(hFile1, message, sizeOfBuffer, &cbWrite, NULL);
		if (!fSuccess) {
			_tprintf(TEXT("Unexpected error[Write to message file failed. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, msg);
			HeapFree(hHeap, 0, from);
			HeapFree(hHeap, 0, filePath);
			HeapFree(hHeap, 0, message);
			CloseHandle(hFile1);
			return;
		}
		CloseHandle(hFile1);
		StringCbCopy(filePath, 256 * sizeof(TCHAR), from);
		StringCbCat(filePath, 256 * sizeof(TCHAR), TEXT("Istoric.txt"));
		StringCbCopy(message, 256 * sizeof(TCHAR), TEXT("To "));
		StringCbCat(message, 256 * sizeof(TCHAR), user);
		StringCbCat(message, 256 * sizeof(TCHAR), TEXT(": "));
		StringCbCat(message, 256 * sizeof(TCHAR), msg);
		StringCbCat(message, 256 * sizeof(TCHAR), TEXT("\r\n"));
		HANDLE hFile2 = CreateFile(filePath, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile2 == INVALID_HANDLE_VALUE) {
			_tprintf(TEXT("Unexpected error[Can not open history file. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, msg);
			HeapFree(hHeap, 0, from);
			HeapFree(hHeap, 0, filePath);
			HeapFree(hHeap, 0, message);
			return;
		}
		SetFilePointer(hFile2, 0, NULL, FILE_END);
		StringCbLength(message, 256 * sizeof(TCHAR), &sizeOfBuffer);
		fSuccess = WriteFile(hFile2, message, sizeOfBuffer, &cbWrite, NULL);
		if (!fSuccess) {
			_tprintf(TEXT("Unexpected error[Write to message file failed. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, user);
			HeapFree(hHeap, 0, msg);
			HeapFree(hHeap, 0, from);
			HeapFree(hHeap, 0, filePath);
			HeapFree(hHeap, 0, message);
			CloseHandle(hFile2);
			return;
		}
		CloseHandle(hFile2);
		
		StringCchCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("YES"));
		pchReply->destination = CLIENT;
		pchReply->type = MESSAGE;
		
		HeapFree(hHeap, 0, user);
		HeapFree(hHeap, 0, msg);
		HeapFree(hHeap, 0, from);
		HeapFree(hHeap, 0, filePath);
		HeapFree(hHeap, 0, message);
		CloseHandle(hFile1);
		return;
	}
	//////////////////////////////////////BROADCAST/////////////////////////////
	else if (pchRequest->type == BROADCAST) {
		DWORD sizeOfBuffer;
		//we send the message to the logged users and we keep the message for the other ones in their 
		//userMesaje.txt file
		LPTSTR sender = (LPTSTR)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == sender) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			return;
		}
		EnterCriticalSection(&accountsLock);
		for (int i = 0;i < numOfAcc;i++) {
			BOOL logged = FALSE;
			EnterCriticalSection(&activeAccountsLock);
			for (int j = 0;j < numOfActiveAcc;j++) {
				//check for the sender
				if ((activeAccounts + j)->hPipe == hPipe && !wcscmp((activeAccounts + j)->user,(accounts + i)->user)) {
					logged = TRUE;
					StringCbCopy(sender, 255 * sizeof(TCHAR), (activeAccounts + j)->user);
					break;
				}
				//if the user is active, we send him the message and put it in the history 
				if (!wcscmp((activeAccounts + j)->user, (accounts + i)->user)) {
					logged = TRUE;
					INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
					if (NULL == infos) {
						_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
						HeapFree(hHeap, 0, sender);
						HeapFree(hHeap, 0, infos);
						LeaveCriticalSection(&accountsLock);
						LeaveCriticalSection(&activeAccountsLock);
						return;
					}
					infos->destination = CLIENT;
					infos->type = RECEIVED;
					StringCbCopy(infos->message, 256 * sizeof(TCHAR), TEXT("Broadcast from "));
					StringCbCat(infos->message, 256 * sizeof(TCHAR), sender);
					StringCbCat(infos->message, 256 * sizeof(TCHAR), TEXT(": "));
					StringCbCat(infos->message, 256 * sizeof(TCHAR), pchRequest->message);
					StringCbCat(infos->message, 256 * sizeof(TCHAR), TEXT("\r\n"));
					DWORD cbWritten;
					BOOL fSuccess = WriteFile((activeAccounts + j)->hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
					if (!fSuccess)
					{
						_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
						HeapFree(hHeap, 0, sender);
						HeapFree(hHeap, 0, infos);
						LeaveCriticalSection(&accountsLock);
						LeaveCriticalSection(&activeAccountsLock);
						return;
					}
					HeapFree(hHeap, 0, infos);
					//put in the history
					LPTSTR filePath = (LPTSTR)HeapAlloc(hHeap, 0, 256 * sizeof(TCHAR));
					if (NULL == filePath) {
						_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
						HeapFree(hHeap, 0, sender);
						LeaveCriticalSection(&accountsLock);
						LeaveCriticalSection(&activeAccountsLock);
						return;
					}
					LPTSTR message = (LPTSTR)HeapAlloc(hHeap, 0, 256 * sizeof(TCHAR));
					if (NULL == message) {
						_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
						HeapFree(hHeap, 0, filePath);
						HeapFree(hHeap, 0, sender);
						LeaveCriticalSection(&accountsLock);
						LeaveCriticalSection(&activeAccountsLock);
						return;
					}
					StringCbCopy(filePath, 256 * sizeof(TCHAR), (activeAccounts + j)->user);
					StringCbCat(filePath, 256 * sizeof(TCHAR), TEXT("Istoric.txt"));
					StringCbCopy(message, 256 * sizeof(TCHAR), TEXT("From "));
					StringCbCat(message, 256 * sizeof(TCHAR), sender);
					StringCbCat(message, 256 * sizeof(TCHAR), TEXT(": "));
					StringCbCat(message, 256 * sizeof(TCHAR), pchRequest->message);
					StringCbCat(message, 256 * sizeof(TCHAR), TEXT("\r\n"));
					HANDLE hFile1 = CreateFile(filePath, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
					if (hFile1 == INVALID_HANDLE_VALUE) {
						_tprintf(TEXT("Unexpected error[Can not open history file. Error: %x]\n"), GetLastError());
						HeapFree(hHeap, 0, filePath);
						HeapFree(hHeap, 0, sender);
						HeapFree(hHeap, 0, message);
						LeaveCriticalSection(&accountsLock);
						LeaveCriticalSection(&activeAccountsLock);
						return;
					}
					SetFilePointer(hFile1, 0, NULL, FILE_END);
					DWORD cbWrite;
					fSuccess = WriteFile(hFile1, message, 256 * sizeof(TCHAR), &cbWrite, NULL);
					if (!fSuccess) {
						_tprintf(TEXT("Unexpected error[Write to message file failed. Error: %x]\n"), GetLastError());
						HeapFree(hHeap, 0, filePath);
						HeapFree(hHeap, 0, sender);
						HeapFree(hHeap, 0, message);
						LeaveCriticalSection(&accountsLock);
						LeaveCriticalSection(&activeAccountsLock);
						CloseHandle(hFile1);
						return;
					}
					CloseHandle(hFile1);
					StringCbCopy(filePath, 256 * sizeof(TCHAR), sender);
					StringCbCat(filePath, 256 * sizeof(TCHAR), TEXT("Istoric.txt"));
					StringCbCopy(message, 256 * sizeof(TCHAR), TEXT("To "));
					StringCbCat(message, 256 * sizeof(TCHAR), (activeAccounts + j)->user);
					StringCbCat(message, 256 * sizeof(TCHAR), TEXT(": "));
					StringCbCat(message, 256 * sizeof(TCHAR), pchRequest->message);
					StringCbCat(message, 256 * sizeof(TCHAR), TEXT("\r\n"));
					HANDLE hFile2 = CreateFile(filePath, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
					if (hFile2 == INVALID_HANDLE_VALUE) {
						_tprintf(TEXT("Unexpected error[Can not open history file. Error: %x]\n"), GetLastError());
						HeapFree(hHeap, 0, filePath);
						HeapFree(hHeap, 0, sender);
						HeapFree(hHeap, 0, message);
						LeaveCriticalSection(&accountsLock);
						LeaveCriticalSection(&activeAccountsLock);
						return;
					}
					SetFilePointer(hFile2, 0, NULL, FILE_END);
					cbWrite;
					fSuccess = WriteFile(hFile2, message, 256 * sizeof(TCHAR), &cbWrite, NULL);
					if (!fSuccess) {
						_tprintf(TEXT("Unexpected error[Write to message file failed. Error: %x]\n"), GetLastError());
						HeapFree(hHeap, 0, filePath);
						HeapFree(hHeap, 0, sender);
						HeapFree(hHeap, 0, message);
						LeaveCriticalSection(&accountsLock);
						LeaveCriticalSection(&activeAccountsLock);
						CloseHandle(hFile2);
						return;
					}
					HeapFree(hHeap, 0, filePath);
					HeapFree(hHeap, 0, message);
					CloseHandle(hFile2);
					break;
				}
			}
			LeaveCriticalSection(&activeAccountsLock);
			if (logged == FALSE) {
				LPTSTR filePath = (LPTSTR)HeapAlloc(hHeap, 0, 256 * sizeof(TCHAR));
				if (NULL == filePath) {
					_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
					HeapFree(hHeap, 0, sender);
					LeaveCriticalSection(&accountsLock);
					return;
				}
				LPTSTR message = (LPTSTR)HeapAlloc(hHeap, 0, 256 * sizeof(TCHAR));
				if (NULL == message) {
					_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
					HeapFree(hHeap, 0, filePath);
					HeapFree(hHeap, 0, sender);
					HeapFree(hHeap, 0, message);
					LeaveCriticalSection(&accountsLock);
					return;
				}
				StringCbCopy(filePath, 256 * sizeof(TCHAR), (accounts + i)->user);
				StringCbCat(filePath, 256 * sizeof(TCHAR), TEXT("Mesaje.txt"));
				StringCbCopy(message, 256 * sizeof(TCHAR), TEXT("Broadcast from "));
				StringCbCat(message, 256 * sizeof(TCHAR), sender);
				StringCbCat(message, 256 * sizeof(TCHAR), TEXT(": "));
				StringCbCat(message, 256 * sizeof(TCHAR), pchRequest->message);
				StringCbCat(message, 256 * sizeof(TCHAR), TEXT("\r\n"));
				HANDLE hFile1 = CreateFile(filePath, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
				if (hFile1 == INVALID_HANDLE_VALUE) {
					_tprintf(TEXT("Unexpected error[Can not open history file. Error: %x]\n"), GetLastError());
					HeapFree(hHeap, 0, filePath);
					HeapFree(hHeap, 0, sender);
					HeapFree(hHeap, 0, message);
					LeaveCriticalSection(&accountsLock);
					return;
				}
				SetFilePointer(hFile1, 0, NULL, FILE_END);
				DWORD cbWrite;
				BOOL fSuccess = WriteFile(hFile1, message, 256 * sizeof(TCHAR), &cbWrite, NULL);
				if (!fSuccess) {
					_tprintf(TEXT("Unexpected error[Write to message file failed. Error: %x]\n"), GetLastError());
					HeapFree(hHeap, 0, filePath);
					HeapFree(hHeap, 0, sender);
					HeapFree(hHeap, 0, message);
					LeaveCriticalSection(&accountsLock);
					CloseHandle(hFile1);
					return;
				}
				CloseHandle(hFile1);
				StringCbCopy(filePath, 256 * sizeof(TCHAR), (accounts + i)->user);
				StringCbCat(filePath, 256 * sizeof(TCHAR), TEXT("Istoric.txt"));
				StringCbCopy(message, 256 * sizeof(TCHAR), TEXT("From "));
				StringCbCat(message, 256 * sizeof(TCHAR), sender);
				StringCbCat(message, 256 * sizeof(TCHAR), TEXT(": "));
				StringCbCat(message, 256 * sizeof(TCHAR), pchRequest->message);
				StringCbCat(message, 256 * sizeof(TCHAR), TEXT("\r\n"));
				HANDLE hFile2 = CreateFile(filePath, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
				if (hFile2 == INVALID_HANDLE_VALUE) {
					_tprintf(TEXT("Unexpected error[Can not open history file. Error: %x]\n"), GetLastError());
					HeapFree(hHeap, 0, filePath);
					HeapFree(hHeap, 0, sender);
					HeapFree(hHeap, 0, message);
					LeaveCriticalSection(&accountsLock);
					return;
				}
				SetFilePointer(hFile2, 0, NULL, FILE_END);
				StringCbLength(message, 256 * sizeof(TCHAR), &sizeOfBuffer);
				fSuccess = WriteFile(hFile2, message, sizeOfBuffer, &cbWrite, NULL);
				if (!fSuccess) {
					_tprintf(TEXT("Unexpected error[Write to message file failed. Error: %x]\n"), GetLastError());
					HeapFree(hHeap, 0, filePath);
					HeapFree(hHeap, 0, sender);
					HeapFree(hHeap, 0, message);
					LeaveCriticalSection(&accountsLock);
					CloseHandle(hFile2);
					return;
				}
				CloseHandle(hFile2);
				StringCbCopy(filePath, 256 * sizeof(TCHAR), sender);
				StringCbCat(filePath, 256 * sizeof(TCHAR), TEXT("Istoric.txt"));
				StringCbCopy(message, 256 * sizeof(TCHAR), TEXT("To "));
				StringCbCat(message, 256 * sizeof(TCHAR), (accounts + i)->user);
				StringCbCat(message, 256 * sizeof(TCHAR), TEXT(": "));
				StringCbCat(message, 256 * sizeof(TCHAR), pchRequest->message);
				StringCbCat(message, 256 * sizeof(TCHAR), TEXT("\r\n"));
				HANDLE hFile3 = CreateFile(filePath, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
				if (hFile3 == INVALID_HANDLE_VALUE) {
					_tprintf(TEXT("Unexpected error[Can not open history file. Error: %x]\n"), GetLastError());
					HeapFree(hHeap, 0, filePath);
					HeapFree(hHeap, 0, sender);
					HeapFree(hHeap, 0, message);
					LeaveCriticalSection(&accountsLock);
					return;
				}
				SetFilePointer(hFile3, 0, NULL, FILE_END);
				StringCbLength(message, 256 * sizeof(TCHAR), &sizeOfBuffer);
				fSuccess = WriteFile(hFile3, message, sizeOfBuffer, &cbWrite, NULL);
				if (!fSuccess) {
					_tprintf(TEXT("Unexpected error[Write to message file failed. Error: %x]\n"), GetLastError());
					HeapFree(hHeap, 0, filePath);
					HeapFree(hHeap, 0, sender);
					HeapFree(hHeap, 0, message);
					LeaveCriticalSection(&accountsLock);
					CloseHandle(hFile3);
					return;
				}
				CloseHandle(hFile3);
				HeapFree(hHeap, 0, filePath);
				HeapFree(hHeap, 0, message);
			}
		}
		LeaveCriticalSection(&accountsLock);
		pchReply->destination = CLIENT;
		pchReply->type = BROADCAST;
		HeapFree(hHeap, 0, sender);
		return;
	}
	///////////////////////////////HISTORY/////////////////////////
	else if (pchRequest->type == HISTORY) {
		LPTSTR to = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == to) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			return;
		}
		LPTSTR msg = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == msg) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, to);
			return;
		}
		LPTSTR row1;
		LPTSTR rowstate1;
		row1 = wcstok_s(pchRequest->message, TEXT(" "), &rowstate1);
		StringCbCopy(to, 255 * sizeof(TCHAR), row1);
		row1 = wcstok_s(NULL, TEXT("\n"), &rowstate1);
		StringCbCopy(msg, 255 * sizeof(TCHAR), row1);
		DWORD count = _wtoi(msg);
		//count is not a number => invalid command
		if (!count) {
			pchReply->destination = CLIENT;
			pchReply->type = HISTORY;
			StringCbCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("INVALID"));
			HeapFree(hHeap, 0, to);
			HeapFree(hHeap, 0, msg);
			return ;
		}
		HeapFree(hHeap, 0, msg);
		//we search to see if the user exists
		BOOL exists = FALSE;
		EnterCriticalSection(&accountsLock);
		for (int i = 0;i < numOfAcc;i++) {
			if (!wcscmp((accounts + i)->user, to)) {
				exists = TRUE;
				break;
			}
		}
		LeaveCriticalSection(&accountsLock);
		if (exists == FALSE) {
			pchReply->destination = CLIENT;
			pchReply->type = HISTORY;
			StringCbCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("NO"));
			HeapFree(hHeap, 0, to);
			return;
		}
		LPTSTR filePath = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == filePath) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, to);
			return;
		}
		EnterCriticalSection(&activeAccountsLock);
		for (int i = 0;i < numOfActiveAcc;i++) {
			if ((activeAccounts + i)->hPipe == hPipe) {
				StringCbCopy(filePath, 255 * sizeof(TCHAR), (activeAccounts + i)->user);
				break;
			}
		}
		LeaveCriticalSection(&activeAccountsLock);
		StringCbCat(filePath, 255 * sizeof(TCHAR), TEXT("Istoric.txt"));
		HANDLE hFile = CreateFile(filePath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		if (hFile == INVALID_HANDLE_VALUE) {
			_tprintf(TEXT("Unexpected error[Cannot open registration file. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, to);
			HeapFree(hHeap, 0, filePath);
			return;
		}
		//we take all the file and tokenize it
		PLARGE_INTEGER fileSize = (PLARGE_INTEGER)HeapAlloc(hHeap, 0, sizeof(LARGE_INTEGER));
		if (NULL == fileSize) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			CloseHandle(hFile);
			HeapFree(hHeap, 0, to);
			HeapFree(hHeap, 0, filePath);
			return;
		}
		LPTSTR buffer = (LPTSTR)HeapAlloc(hHeap, 0, sizeof(TCHAR) * fileSize->LowPart + sizeof(TCHAR));
		if (NULL == buffer) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			CloseHandle(hFile);
			HeapFree(hHeap, 0, fileSize);
			HeapFree(hHeap, 0, to);
			HeapFree(hHeap, 0, filePath);
			return;
		}
		DWORD cbRead;

		if (!GetFileSizeEx(hFile, fileSize)) {
			_tprintf(TEXT("Unexpected error[Cannot get registration file size. Error: %x]\n"), GetLastError());
			CloseHandle(hFile);
			HeapFree(hHeap, 0, fileSize);
			HeapFree(hHeap, 0, to);
			HeapFree(hHeap, 0, filePath);
			HeapFree(hHeap, 0, buffer);
			return;
		}
		
		if (!ReadFile(hFile, buffer, sizeof(TCHAR) * fileSize->LowPart, &cbRead, NULL)) {
			_tprintf(TEXT("Unexpected error[Cannot read from registration file. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, fileSize);
			HeapFree(hHeap, 0, to);
			HeapFree(hHeap, 0, filePath);
			HeapFree(hHeap, 0, buffer);
			CloseHandle(hFile);
			return;
		}
		CloseHandle(hFile);
		HeapFree(hHeap, 0, filePath);
		hFile = NULL;
		//a message To a: b\r\n has 9 chars
		//worst case scenario, all messages have 9 chars, so we need fileSize / 9 + 1 buffers to store all the messages
		DWORD size = fileSize->LowPart / (9 * sizeof(TCHAR)) + 1;
		HeapFree(hHeap, 0, fileSize);
		DWORD numOfMessages = 0;
		TCHAR** buffers = (TCHAR**)HeapAlloc(hHeap, 0, size * sizeof(TCHAR*));
		if (buffers == NULL) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, to);
			HeapFree(hHeap, 0, filePath);
			HeapFree(hHeap, 0, buffer);
			return;
		}
		LPTSTR row;
		LPTSTR rowstate;
		row = wcstok_s(buffer, TEXT("\n"), &rowstate);
		buffers[numOfMessages] = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR*));
		if (buffers[numOfMessages] == NULL) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, to);
			HeapFree(hHeap, 0, filePath);
			HeapFree(hHeap, 0, buffer);
			HeapFree(hHeap, 0, buffers);
			return;
		}
		DWORD s = 0;
		StringCchLength(row, 255, &s);
		StringCbCopy(buffers[numOfMessages], s * sizeof(TCHAR), row);
		numOfMessages++;
		while (s > 0) {
			row = wcstok_s(NULL, TEXT("\n"), &rowstate);
			StringCchLength(row, 255, &s);
			if (s > 0 ) {
				buffers[numOfMessages] = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR*));
				if (buffers[numOfMessages] == NULL) {
					_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
					HeapFree(hHeap, 0, to);
					HeapFree(hHeap, 0, filePath);
					HeapFree(hHeap, 0, buffer);
					for (int i = 0;i < numOfMessages;i++) {
						HeapFree(hHeap, 0, buffers[i]);
					}
					HeapFree(hHeap, 0, buffers);
					return;
				}
				StringCbCopy(buffers[numOfMessages], s * sizeof(TCHAR), row);
				numOfMessages++;
			}
		}
		//here we have in buffers every message of the sender
		INT contor = numOfMessages - 1;
		DWORD sizeOfDestinatar;
		DWORD numOfSent = 0;
		StringCchLength(to, 255, &sizeOfDestinatar);
		INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
		if (NULL == infos) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, to);
			HeapFree(hHeap, 0, filePath);
			HeapFree(hHeap, 0, buffer);
			for (int i = 0;i < numOfMessages;i++) {
				HeapFree(hHeap, 0, buffers[i]);
			}
			HeapFree(hHeap, 0, buffers);
			return;
		}
		while (contor >= 0 && count > 0) {
			//two types of messages, one starts with To and one starts with From
			if (!wcsncmp(buffers[contor], TEXT("To"), 2)) {
				//the name of the destinatar starts on 4th position
				if (!wcsncmp(to, buffers[contor] + 3, sizeOfDestinatar)) {
					infos->destination = CLIENT;
					infos->type = RECEIVED;
					StringCbCopy(infos->message, 255 * sizeof(TCHAR), buffers[contor]);
					DWORD cbWritten;
					BOOL fSuccess = WriteFile(hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
					if (!fSuccess)
					{
						_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
						HeapFree(hHeap, 0, to);
						HeapFree(hHeap, 0, filePath);
						HeapFree(hHeap, 0, buffer);
						for (int i = 0;i < numOfMessages;i++) {
							HeapFree(hHeap, 0, buffers[i]);
						}
						HeapFree(hHeap, 0, buffers);
						HeapFree(hHeap, 0, infos);
						return;
					}
					count--;
					numOfSent++;
				}
				//otherwise, we go to the next message
				contor--;
			}
			else {
				//the name of the destinatar starts on 6th position
				if (!wcsncmp(to, buffers[contor] + 5, sizeOfDestinatar)) {
					infos->destination = CLIENT;
					infos->type = RECEIVED;
					StringCbCopy(infos->message, 255 * sizeof(TCHAR), buffers[contor]);
					DWORD cbWritten;
					BOOL fSuccess = WriteFile(hPipe, infos, sizeof(INFOS), &cbWritten, NULL);
					if (!fSuccess)
					{
						_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
						HeapFree(hHeap, 0, to);
						HeapFree(hHeap, 0, filePath);
						HeapFree(hHeap, 0, buffer);
						for (int i = 0;i < numOfMessages;i++) {
							HeapFree(hHeap, 0, buffers[i]);
						}
						HeapFree(hHeap, 0, buffers);
						HeapFree(hHeap, 0, infos);
						return;
					}
					count--;
					numOfSent++;
				}
				//otherwise, we go to the next message
				contor--;
			}
		}
		//if there is no message between the two users, we send a newline
		if (numOfSent == 0) {
			infos->destination = CLIENT;
			infos->type = HISTORY;
			StringCbCopy(infos->message, 255 * sizeof(TCHAR), TEXT("\n"));
			DWORD cbWritten4;
			BOOL fSuccess4 = WriteFile(hPipe, infos, sizeof(INFOS), &cbWritten4, NULL);
			if (!fSuccess4)
			{
				_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
				HeapFree(hHeap, 0, to);
				HeapFree(hHeap, 0, filePath);
				HeapFree(hHeap, 0, buffer);
				for (int i = 0;i < numOfMessages;i++) {
					HeapFree(hHeap, 0, buffers[i]);
				}
				HeapFree(hHeap, 0, buffers);
				HeapFree(hHeap, 0, infos);
				return;
			}
		}
		pchReply->destination = CLIENT;
		pchReply->type = HISTORY;
		StringCbCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("DONE"));
		HeapFree(hHeap, 0, to);
		HeapFree(hHeap, 0, buffer);
		for (int i = 0;i < numOfMessages;i++) {
			HeapFree(hHeap, 0, buffers[i]);
		}
		HeapFree(hHeap, 0, buffers);
		HeapFree(hHeap, 0, infos);
		return;
	}
	/////////////////////////////////SENDFILE///////////////////////
	else if (pchRequest->type == SENDFILE) {
		LPTSTR user;
		LPTSTR path;
		//additional contains the name of the destinatar and the full relative path
		user = wcstok_s(pchRequest->additional, TEXT(" "), &path);
		//in path we have the full relative path from the sender, but we want only the name of the file
		//so the string after the last /
		DWORD size;
		StringCchLength(path, 255, &size);
		size--;
		BOOL found = FALSE;
		while (found == FALSE && size > 0) {
			if (!wcsncmp((path + size), TEXT("\\"), 1)) {
				size++;
				found = TRUE;
			}
			if (found == FALSE) {
				size--;
			}
		}
		LPTSTR filePath = (LPTSTR)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == filePath) {
			_tprintf(TEXT("Unexpected error: [No more space on Heap]"));
			return;
		}
		StringCbCopy(filePath, 255 * sizeof(TCHAR), path + size);
		//we verify if the user is registered
		BOOL exists = FALSE;
		EnterCriticalSection(&accountsLock);
		for (int i = 0;i < numOfAcc;i++) {
			if (!wcscmp((accounts + i)->user, user)) {
				exists = TRUE;
				break;
			}
		}
		LeaveCriticalSection(&accountsLock);
		if (exists == FALSE) {
			//we send one message that say that there is no such user, for example the one for the history command
			pchReply->destination = CLIENT;
			pchReply->type = HISTORY;
			StringCbCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("NO"));
			HeapFree(hHeap, 0, filePath);
			return;
		}
		//otherwise, we search to see if the user is online
		exists = FALSE;
		HANDLE hPipe1 = NULL;
		EnterCriticalSection(&activeAccountsLock);
		for (int i = 0;i < numOfActiveAcc;i++) {
			if (!wcscmp((activeAccounts + i)->user, user)) {
				//if we found the user, we need his pipe handle so we can send the message
				exists = TRUE;
				hPipe1 = (activeAccounts + i)->hPipe;
				break;
			}
		}
		LeaveCriticalSection(&activeAccountsLock);
		if (exists == FALSE) {
			//we send one message that say that there is no user online
			pchReply->destination = CLIENT;
			pchReply->type = SENDFILE;
			StringCbCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("NO"));
			HeapFree(hHeap, 0, filePath);
			return;
		}
		//if we reach this line, it means that everything is ok, and we can send the buffer to the client specified
		INFOS* infos = (INFOS*)HeapAlloc(hHeap, 0, sizeof(INFOS));
		if (NULL == infos) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, filePath);
			return;
		}
		infos->destination = CLIENT;
		infos->type = SENDFILE;
		StringCbCopy(infos->message, 256 * sizeof(TCHAR), pchRequest->message);
		StringCbCopy(infos->additional, 256 * sizeof(TCHAR), filePath);
		DWORD cbWritten;
		BOOL fSuccess = WriteFile(hPipe1, infos, sizeof(INFOS), &cbWritten, NULL);
		if (!fSuccess)
		{
			_tprintf(TEXT("Unexpected error: [WriteFile to pipe failed. GLE=%d]\n"), GetLastError());
			HeapFree(hHeap, 0, filePath);
			HeapFree(hHeap, 0, infos);
			return;
		}
		HeapFree(hHeap, 0, infos);
		HeapFree(hHeap, 0, filePath);

		//we send to the client a DO NOTHING command
		pchReply->destination = CLIENT;
		pchReply->type = HISTORY;
		StringCbCopy(pchReply->message, 255 * sizeof(TCHAR), TEXT("DONE"));
		return;
	}
	else if (pchRequest->type == DONE) {
		pchReply->type = DONE;
		pchReply->destination = CLIENT;
	}
}

VOID GetRegisteredClients() {
	HANDLE hHeap = GetProcessHeap();
	activeAccounts = (ACTIVE_ACCOUNT*)HeapAlloc(hHeap, 0, activeAccountSize * sizeof(ACTIVE_ACCOUNT));
	if (NULL == activeAccounts) {
		_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
		return;
	}
	HANDLE hFile;
	hFile = CreateFile(TEXT("C:\\registration.txt"),
		GENERIC_READ,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);
	if (hFile == INVALID_HANDLE_VALUE) {
		_tprintf(TEXT("Unexpected error[Cannot open registration file. Error: %x]\n"), GetLastError());
		CloseHandle(hFile);
		return;
	}
	PLARGE_INTEGER fileSize = (PLARGE_INTEGER)HeapAlloc(hHeap, 0, sizeof(LARGE_INTEGER));
	if (NULL == fileSize) {
		_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
		CloseHandle(hFile);
		return;
	}
	DWORD cbRead;
	TCHAR* buffer = (TCHAR*)HeapAlloc(hHeap, 0, sizeof(TCHAR) * fileSize->LowPart + sizeof(TCHAR));
	if (NULL == buffer) {
		HeapFree(hHeap, 0, fileSize);
		_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
		CloseHandle(hFile);
		return;
	}
	EnterCriticalSection(&regLock);
	if (!GetFileSizeEx(hFile, fileSize)) {
		_tprintf(TEXT("Unexpected error[Cannot get registration file size. Error: %x]\n"), GetLastError());
		HeapFree(hHeap, 0, fileSize);
		HeapFree(hHeap, 0, buffer);
		CloseHandle(hFile);
		LeaveCriticalSection(&regLock);
		return;
	}
	if (!ReadFile(hFile, buffer, sizeof(TCHAR) * fileSize->LowPart, &cbRead, NULL)) {
		_tprintf(TEXT("Unexpected error[Cannot read from registration file. Error: %x]\n"), GetLastError());
		HeapFree(hHeap, 0, fileSize);
		HeapFree(hHeap, 0, buffer);
		CloseHandle(hFile);
		LeaveCriticalSection(&regLock);
		return;
	}
	LeaveCriticalSection(&regLock);

	HeapFree(hHeap, 0, fileSize);
	accounts = (ACCOUNT*)HeapAlloc(hHeap, 0, accountSize * sizeof(ACCOUNT));
	if (NULL == accounts) {
		_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
		HeapFree(hHeap, 0, buffer);
		CloseHandle(hFile);
		return;
	}
	LPTSTR row;
	LPTSTR rowstate;
	row = wcstok_s(buffer, TEXT("\r"), &rowstate);
	while ((*(row + 1) >= 'a' && *(row + 1) <= 'z') || (*(row + 1) >= 'A' && *(row + 1) <= 'Z')) {
		LPTSTR column;
		LPTSTR columnstate;
		column = wcstok_s(row + 1, TEXT(","), &columnstate);
		(accounts + numOfAcc)->user = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == (accounts + numOfAcc)->user) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, buffer);
			CloseHandle(hFile);
			return;
		}
		StringCbCopy((accounts + numOfAcc)->user, 256 * sizeof(TCHAR), column);
		column = wcstok_s(NULL, TEXT(","), &columnstate);
		(accounts + numOfAcc)->password = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
		if (NULL == (accounts + numOfAcc)->password) {
			_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
			HeapFree(hHeap, 0, buffer);
			CloseHandle(hFile);
			return;
		}
		StringCbCopy((accounts + numOfAcc)->password, 256 * sizeof(TCHAR), column);
		numOfAcc++;
		row = wcstok_s(NULL, TEXT("\r"), &rowstate);
		if (numOfAcc == accountSize - 1) {
			ACCOUNT* temp = (ACCOUNT*)HeapAlloc(hHeap, 0, accountSize * 2 * sizeof(ACCOUNT));
			if (temp == NULL) {
				_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
				HeapFree(hHeap, 0, buffer);
				CloseHandle(hFile);
				return;
			}
			for (int i = 0;i < accountSize;i++) {
				(temp + i)->user = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
				if (NULL == (temp + i)->user) {
					_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
					HeapFree(hHeap, 0, buffer);
					CloseHandle(hFile);
					return;
				}
				StringCbCopy((temp + i)->user, 256 * sizeof(TCHAR), (accounts + i)->user);
				(temp + i)->password = (TCHAR*)HeapAlloc(hHeap, 0, 255 * sizeof(TCHAR));
				if (NULL == (temp + i)->password) {
					_tprintf(TEXT("Unexpected error[No more space on heap. Error: %x]\n"), GetLastError());
					HeapFree(hHeap, 0, buffer);
					CloseHandle(hFile);
					return;
				}
				StringCbCopy((temp + i)->password, 256 * sizeof(TCHAR), (accounts + i)->password);
			}
			HeapFree(hHeap, 0, accounts);
			accounts = temp;
			temp = NULL;
			accountSize *= 2;
		}
	}
	CloseHandle(hFile);
}

